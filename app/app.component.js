"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Math = (function () {
    function Math() {
    }
    return Math;
}());
exports.Math = Math;
var AppComponent = (function () {
    function AppComponent() {
        this.title = 'My first project';
        this.math = {
            x: "",
            y: "",
            sum: ""
        };
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n    <h1>My First Project!</h1>\n    <h2>Add these two numbers!</h2>\n    <div><label>x: </label>\n      <input [(ngModel)]=\"math.x\" placeholder=\"Enter 1st Number Here\">\n    <p>\n      <label>y: </label>\n      <input [(ngModel)]=\"math.y\" placeholder=\"Enter 2nd Number Here\">\n    </p>\n    <p>\n      <label>Sum: </label>\n      <input [(ngModel)]=\"math.x -- math.y\" placeholder=\"Sum of Numbers\">\n    </p>\n    "
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map