import { Component } from '@angular/core';
export class Math {
  x: number;
  y: number;
  sum: number;
}
@Component({
  selector: 'my-app',
  template:`
    <h1>My First Project!</h1>
    <h2>Add these two numbers!</h2>
    <div><label>x: </label>
      <input [(ngModel)]="math.x" placeholder="Enter 1st Number Here">
    <p>
      <label>y: </label>
      <input [(ngModel)]="math.y" placeholder="Enter 2nd Number Here">
    </p>
    <p>
      <label>Sum: </label>
      <input [(ngModel)]="math.x -- math.y" placeholder="Sum of Numbers">
    </p>
    `
})
export class AppComponent {
  title = 'My first project';
  math: Math = {
    x: "",
    y: "",
    sum: ""
  };
}
